MyDbFinder
===================

This project documents the MyDbFinder service


Documentation
=============

The MyDbFinder service contains one python script *mydbfinder.py* and *curate_database.py* which is the scripts of the latest
version of the MyDbFinder service. The service identifies sequences (in fasta or fastq) in databases provided by the user. 

## Content of the repository
1. mydbfinder.py      - the program that does the search
2. curate_database.py	- the program that curates the database before being used by mydbfinder.py
3. README.md


## Installation

Setting up MyDbFinder program
```bash
# Go to wanted location for mydbfinder
cd /path/to/some/dir
# Clone and enter the mydbfinder directory
git clone https://bitbucket.org/genomicepidemiology/mydbfinder.git
cd mydbfinder
```


If kma_index has no bin install please install kma_index from the kma repository:
https://bitbucket.org/genomicepidemiology/kma

## Usage

The program can be invoked with the -h option to get help and more information of the service.
Run Docker container:


```bash
# Run curate database (with kma option if database is used for fastq files)
python curate_database.py [-h] -i INPUT_DATABASE -o OUTPUT_PATH
                          [-kp DB_PATH_KMA]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_DATABASE, --input_database INPUT_DATABASE
                        FASTA file database
  -o OUTPUT_PATH, --output_path OUTPUT_PATH
                        Path to output db
  -kp DB_PATH_KMA, --db_path_kma DB_PATH_KMA
                        Path to kma if required
```
```bash
# Run mydbfinder on the curated database
python mydbfinder.py [-h] -i INPUTFILE [INPUTFILE ...] [-o OUT_PATH]
                     [-tmp TMP_DIR] [-mp METHOD_PATH] [-p DB_PATH]
                     [-d DATABASES] [-l MIN_COV] [-t THRESHOLD]
                     [-ao ACQ_OVERLAP] [-x] [-q]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUTFILE [INPUTFILE ...], --inputfile INPUTFILE [INPUTFILE ...]
                        FASTA or FASTQ input files.
  -o OUT_PATH, --outputPath OUT_PATH
                        Path to blast output
  -tmp TMP_DIR, --tmp_dir TMP_DIR
                        Temporary directory for storage of the results from
                        the external software.
  -mp METHOD_PATH, --methodPath METHOD_PATH
                        Path to method to use (kma or blastn)
  -p DB_PATH, --databasePath DB_PATH
                        Path to the databases
  -d DATABASES, --databases DATABASES
                        Databases chosen to search in - if none are specified
                        all are used
  -l MIN_COV, --min_cov MIN_COV
                        Minimum coverage
  -t THRESHOLD, --threshold THRESHOLD
                        Blast threshold for identity
  -ao ACQ_OVERLAP, --acq_overlap ACQ_OVERLAP
                        Genes are allowed to overlap this number of
                        nucleotides. Default: 30.
  -x, --extented_output
                        Give extented output with allignment files, template
                        and query hits in fasta and a tab seperated file with
                        gene profile results
  -q, --quiet

```

## Web-server

A webserver implementing the methods is available at the [CGE website](http://www.genomicepidemiology.org/) and can be found here:
https://cge.cbs.dtu.dk/services/MyDbFinder/

References
=======

1. Camacho C, Coulouris G, Avagyan V, Ma N, Papadopoulos J, Bealer K, Madden TL. BLAST+: architecture and applications. BMC Bioinformatics 2009; 10:421. 
2. Clausen PTLC, Aarestrup FM, Lund O. Rapid and precise alignment of raw reads against redundant databases with KMA. BMC Bioinformatics 2018; 19:307. 

License
=======

Copyright (c) 2014, Ole Lund, Technical University of Denmark
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

