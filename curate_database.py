#!/usr/bin/env python3
from __future__ import division
import os
from argparse import ArgumentParser
from tabulate import tabulate
import collections
import pprint
import json
import gzip
from Bio import SeqIO

from distutils.spawn import find_executable
from cgecore.blaster import Blaster
from cgecore.cgefinder import CGEFinder

class Database():

    def __init__(self, input_database, db_path_out, kma_index=None):
        """
        """
        self.db_path_out = db_path_out
        self.input_database = input_database
        self.db_name = os.path.splitext(os.path.basename(self.input_database))[0]
        self.extension = os.path.splitext(os.path.basename(self.input_database))[1]
        self.kma_index = kma_index

    def write_config(self):
        """
        Write a easy config file
        """
        config_filename = "{}/config".format(self.db_path_out)
        config_file = open(config_filename, "w")
        config_file.write("#db_prefix\tname\tdescription\n")
        config_file.write("{0}\t{0}\t{0}".format(self.db_name))
        config_file.close()
        self.config_filename = config_filename

    def index_db(self):
        """
        Kma index database
        """
        # Use config_file to go through database dirs
        config_file = open(self.config_filename, "r")
        for line in config_file:
            if line.startswith("#"):
                continue
            else:
                line = line.rstrip().split("\t")
                name_db = line[0].strip()
                # for each dir index the fasta files
                os.system("{0} -i {2}/{1}{3} -o {2}/{1}".format(self.kma_index,
                                                                name_db,
                                                                self.db_path_out,
                                                                self.extension))
        config_file.close()

    def check_headers(self):
        """
        Not allow spaces in fasta headers
        """
        os.system("sed 's, ,_,g' -i {}".format(self.input_database))



    def run(self):
        self.check_headers()
        self.write_config()
        if self.kma_index:
            self.index_db()


if __name__ == '__main__':

    ##########################################################################
    # PARSE COMMAND LINE OPTIONS
    ##########################################################################

    parser = ArgumentParser()
    parser.add_argument("-i", "--input_database",
                        dest="input_database",
                        help="FASTA file database",
                        required=True)
    parser.add_argument("-o", "--output_path",
                        dest="output_path",
                        help="Path to output db",
                        required=True)
    parser.add_argument("-kp", "--db_path_kma",
                        dest="db_path_kma",
                        help="Path to kma if required",
                        default=None)

    args = parser.parse_args()

    ##########################################################################
    # MAIN
    ##########################################################################


    c = Database(args.input_database, args.output_path, args.db_path_kma)
    c.run()
